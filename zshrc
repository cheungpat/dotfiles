# Load all files from .zshrc.d directory
if [ -d $HOME/.zshrc.d ]; then
  for file in $HOME/.zshrc.d/*.zsh; do
    source $file
  done
fi

if [ -e ~/.zshrc.local ]; then
    source ~/.zshrc.local
fi 

export PATH="$HOME/.golang/bin:$PATH"



# Transitioning autojump to fasd
alias j='fasd_cd -d'

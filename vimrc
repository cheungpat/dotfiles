call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-fugitive'
Plug 'majutsushi/tagbar'
Plug 'tpope/vim-markdown'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'gisphm/vim-gitignore'
Plug 'mileszs/ack.vim'
Plug 'pangloss/vim-javascript'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'KabbAmine/zeavim.vim'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'editorconfig/editorconfig-vim'
" Syntax autofix
Plug 'chase/vim-ansible-yaml'
Plug 'tomtom/tcomment_vim'

" Fuzzy searching
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Auto insert closing parenthesis
Plug 'Krasjet/auto.pairs'

" Git commits browser
Plug 'junegunn/gv.vim'

" Minimap
Plug 'wfxr/minimap.vim'

" Leap
" Navigate text by typing a few characters of where you want to go.
Plug 'ggandor/leap.nvim'

" COC is for code completion. It is written in NodeJS but can be used
" for editing files for other programming languages (via LSP).
" Autofix is provided by extensions. Also check coc-settings.json.
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'antoinemadec/coc-fzf', {'branch': 'release'}

" When pressing certain key, it pops up suggestion after a delay.
Plug 'folke/which-key.nvim'

if has('nvim')
  " Show signs at the beginning of the line for git line status, can also show
  " blame info.
  Plug 'lewis6991/gitsigns.nvim'
else
  Plug 'airblade/vim-gitgutter'
endif

Plug 'folke/trouble.nvim'

Plug 'echasnovski/mini.icons'

call plug#end()

lua << EOF
  require("which-key").setup {
    -- Without this config, certain keys will trigger WhichKey to pop up
    -- immediately. e.g. `"`
    delay = 500
  }
  require('gitsigns').setup()

  require('mini.icons').setup()
EOF

syntax on

set encoding=utf-8
set autoindent

set expandtab
set ts=4
set sw=4

" Wrapping
set wrap
set textwidth=80
" roq - comments
" a - formats paragraphs
" n - recognize numbered list
" l - don't break already long lines before insert
" 1 - don't break line after one letter world.
set formatoptions=roqnl1
set colorcolumn=80
highlight ColorColumn ctermbg=8

" Disable invisibles, disabled because of too distrating.
set list
set listchars=tab:▸\ ,eol:¬
highlight NonText guifg=#4a4a59
highlight SpecialKey guifg=#4a4a59

" Scroll screen with this number of lines as padding.
set scrolloff=3

"" Show relative line number.
""(disabled, this can be slow)
"set relativenumber

" Retain undo history even after buffer is closed.
set undodir=~/.vim/undodir
set undofile

""" Show curosr position
"""(disabled, this can be slow)
""set ruler

"" Draw a line on the line the cursor appears.
""set cursorline

" Keep three lines visible when scrolling.
set scrolloff=3

"" Additional info on the last line.
""(disabled, this can be slow)
"set showmode
"set showcmd

" Ignore case when searching, but search case-sensitive when search string
" contains capital letter.
set ignorecase
set smartcase

" Incremental search
set incsearch
set showmatch
set hlsearch

" ad-hoc
autocmd FileType html setlocal shiftwidth=2 tabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 cino=>2
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2 cino=>2
autocmd FileType typescript setlocal shiftwidth=2 tabstop=2 cino=>2
autocmd FileType typescriptreact setlocal shiftwidth=2 tabstop=2 cino=>2
autocmd BufNewFile,BufRead /Users/patrick/projects/artstack/web/* set sw=2 ts=2

let g:gitgutter_realtime = 0
let g:gitgutter_eager = 0

"let g:tagbar_ctags_bin = '/opt/local/bin/ctags'

"colorscheme ron

hi SpellBad       cterm=bold


map <C-K> :pyf /opt/local/libexec/llvm-3.6/libexec/clang-format/clang-format.py<cr>
imap <C-K> <c-o>:pyf /opt/local/libexec/llvm-3.6/libexec/clang-format/clang-format.py<cr>

set cursorline
hi CursorLine cterm=none ctermbg=52


if executable('rg')
  let g:ackprg = 'rg --vimgrep'
endif

" Use \t or Ctrl-P (old habit) for fzf search
nnoremap <Leader>t :FZF<CR>
nnoremap <c-p> :FZF<CR>
nnoremap <Leader>s :CocFzfList symbols<CR>

for rcfile in split(globpath("~/.vim.d", "*.vim"), '\n') 
    execute('source '.rcfile)
endfor

" Enable mouse mode and make nerd tree respond to revealing detail
set mouse=a
let g:NERDTreeMouseMode=3 

map <silent> <C-n> :NERDTreeToggle<CR>
let NERDTreeRespectWildIgnore=1
" close vim if the only window left open is NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


" Allow backspace to erase indent, especially in python
" https://vi.stackexchange.com/questions/2162/why-doesnt-the-backspace-key-work-in-insert-mode
set backspace=indent,eol,start

" Use arrow keys to select autocomplete filename in command mode
" https://blog.gslin.org/archives/2021/07/11/10235/neovim-在選擇檔案名稱時的操作按鍵/
cnoremap <expr> <Down> pumvisible() ? "\<C-n>" : "\<Down>"
cnoremap <expr> <Up> pumvisible() ? "\<C-p>" : "\<Up>"
"https://unix.stackexchange.com/questions/162528/select-an-item-in-vim-autocomplete-list-without-inserting-line-break
inoremap <expr> <TAB> pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <Esc> pumvisible() ? "\<C-e>" : "\<Esc>"
inoremap <expr> <C-j> pumvisible() ? "\<C-n>" : "\<Down>"
inoremap <expr> <C-k> pumvisible() ? "\<C-p>" : "\<Up>"

" https://pragmaticpineapple.com/ultimate-vim-typescript-setup/
let g:coc_global_extensions = ['coc-tsserver', 'coc-eslint', '@yaegassy/coc-tailwindcss3', 'coc-jedi', 'coc-prettier']
nmap <leader>ac  <Plug>(coc-codeaction)
nmap <leader>qf  <Plug>(coc-fix-current)
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)


" leap
nmap s <Plug>(leap-forward-to)
nmap S <Plug>(leap-backward-to)

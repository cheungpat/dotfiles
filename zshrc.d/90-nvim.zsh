# Replace default editor with Neovim if installed.
if command -v nvim &> /dev/null; then
    alias vim="nvim"
    alias vi="nvim"
    alias oldvim="vim"
    export EDITOR=nvim
fi

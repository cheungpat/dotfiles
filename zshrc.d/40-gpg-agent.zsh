gpg-connect-agent --quiet /bye > /dev/null 2> /dev/null

if [[ "$SSH_AUTH_SOCK" == /private/tmp/com.apple.launchd* ]]; then
    export SSH_AUTH_SOCK=~/.gnupg/S.gpg-agent.ssh
fi

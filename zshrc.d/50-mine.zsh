export LANG=en_US.UTF-8
export EDITOR=vim

compdef ping6=ping

alias ded="rm -rf /Users/$USER/Library/Developer/Xcode/DerivedData"
alias clear-quarantine="sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'delete from LSQuarantineEvent'"
alias fixopenwith="/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user"

unsetopt correct_all
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi
export GOPATH=~/.golang
export HOMEBREW_CACHE=~/Library/Caches/Homebrew

setopt HIST_IGNORE_SPACE
unsetopt share_history

# This reduce the delay after pressing ESC key to enter vi-mode
export KEYTIMEOUT=1

# Press CTRL+W to search history incrementally
bindkey '^r' history-incremental-pattern-search-backward
bindkey '^f' history-incremental-pattern-search-forward

# Bring back some line editing function after using vi-mode
bindkey '^A' beginning-of-line
bindkey '^E' end-of-line

# Prompt
export PROMPT='%(?.🍔 .🍟 )'
export RPROMPT='%{$fg_bold[blue]%}%2~ $(git_prompt_info)%{$reset_color%}'

alias prettyping='prettyping --nolegend'

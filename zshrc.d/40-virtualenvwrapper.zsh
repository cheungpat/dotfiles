export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=$(asdf which python)
source $(dirname $VIRTUALENVWRAPPER_PYTHON)/virtualenvwrapper.sh
